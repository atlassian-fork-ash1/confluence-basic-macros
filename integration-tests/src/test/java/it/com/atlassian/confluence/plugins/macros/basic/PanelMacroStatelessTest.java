package it.com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.pageobjects.elements.PageElement;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;

@RunWith(ConfluenceStatelessTestRunner.class)
public class PanelMacroStatelessTest {

    private static final String MACRO_NAME = "panel";
    private static final String PANEL_PAGE_CONTENT = format("{%s}This is *text* in panel%n{%s}", MACRO_NAME, MACRO_NAME);

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static GlobalElementFinder finder;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture panelPage = pageFixture()
            .space(space)
            .author(user)
            .title("Panel Stateless Test")
            .content(PANEL_PAGE_CONTENT, ContentRepresentation.WIKI)
            .build();

    @BeforeClass
    public static void initialise() {
        product.login(user.get(), DashboardPage.class);
    }

    @Test
    public void testPanel() {
        product.viewPage(panelPage.get());
        PageElement panelContent = finder.find(
                By.xpath("//div[@class='wiki-content']//div[contains(@class,'panel')]//div[@class='panelContent']")
        );
        waitUntil(panelContent.timed().getText(), is("This is text in panel"));
    }

    @Test
    public void testWikiLinkRender() {
        Content content = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .title("Wiki Link Renderer Stateless Test")
                        .type(ContentType.PAGE)
                        .body(
                                format(
                                        "{%s:title=[%s:%s]}This is *text* in panel%n{%s}",
                                        MACRO_NAME,
                                        space.get().getKey(),
                                        panelPage.get().getTitle(),
                                        MACRO_NAME
                                ),
                                ContentRepresentation.WIKI
                        )
                        .build()
        );

        product.viewPage(content);
        PageElement panelHeader = finder.find(By.xpath("//div[@id='main-content']//div[contains(@class, 'panel')]//div[@class='panelHeader']/b/a"));
        waitUntil(panelHeader.timed().getText(), is(panelPage.get().getTitle()));
    }
}
