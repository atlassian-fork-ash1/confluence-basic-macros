package it.com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.pageobjects.elements.PageElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.lang.String.format;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;

@RunWith(ConfluenceStatelessTestRunner.class)
public class AnchorMacroStatelessTest {

    private static final String MACRO_NAME = "anchor";

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static GlobalElementFinder finder;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture anchorPage = pageFixture()
            .space(space)
            .author(user)
            .title("testAnchor")
            .content("", ContentRepresentation.WIKI)
            .build();

    @Test
    public void testAnchor() {
        // can't create page with desired test content as anchors don't work on create (see bug filed here CONFDEV-5680)
        restClient.createSession(user.get()).contentService().update(
                Content.builder()
                        .id(anchorPage.get().getId())
                        .type(ContentType.PAGE)
                        .space(space.get())
                        .title(anchorPage.get().getTitle())
                        .version(anchorPage.get().getVersion().nextBuilder().build())
                        .body(format("[Refer Here|#comments]%n%n{loremipsum:10}%n{%s:comments}%nh3. Notes", MACRO_NAME), ContentRepresentation.WIKI)
                        .build()
        );

        product.loginAndView(user.get(), anchorPage.get());

        PageElement anchorElement = finder.find(By.xpath("//div[@class='wiki-content']//p[1]//a[1]"));
        waitUntil(
                anchorElement.timed().getText(),
                is("Refer Here")
        );
        waitUntil(
               anchorElement.timed().getAttribute("href"),
               // TODO : fix this assertion , it needs to be equalTo
               endsWith("#testAnchor-comments")
        );

        PageElement anchorLink = finder.find(By.xpath("//div[@class='wiki-content']//span[contains(@class, 'confluence-anchor-link')]"));
        waitUntil(
                anchorLink.timed().getAttribute("id"),
                is("testAnchor-comments")
        );
    }
}
